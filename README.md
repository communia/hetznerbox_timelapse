# HetznerBox timelapse

Snapshot planner for hetzner storage boxes

## Quick

clone where it can be safely run and 
`composer install`

then configure:

`cp config/hetznerbox_timelapse.yaml.sample config/hetznerbox_timelapse.yaml`

^edit these settings.

Settings can be override with these environment variables:

```
HB_TIMELAPSE_RETENTION_DAILY
HB_TIMELAPSE_RETENTION_WEEKLY
HB_TIMELAPSE_RETENTION_MONTHLY
HB_TIMELAPSE_CREDENTIALS_USER
HB_TIMELAPSE_CREDENTIALS_PASSWORD
HB_TIMELAPSE_BOX_ID
HB_TIMELAPSE_MOUNT_POINT
```
So it can be used to check multiple box with same app code.

## Rotate command

`main.php snaps:rotate`

Can carry on with the rotation of the snapshots depending on the retention plan
specified in config/hetznerbox_timelapse.yaml .

It can delete existing (non-planned) snapshots using the flag --overwrite.

## List command

`main.php snaps:list`

Can list the snapshots, as a table, or cooked as a check_command to feed
icinga2 or similar monitoring service.

Add ./main.php application in systemd timer or cron job daily.

## Systemd timer

create a `/etc/systemd/system/hetznerboxTimelapse.service` with:

```
# This service unit rotates the snapshots of hetzner storageboxes
# By Aleix Quintana Alsius
# Licensed under GPL V2
#

[Unit]
Description=Rotates the snapshots of hetzner storageboxes
Wants=hetznerboxTimelapse.timer

[Service]
Type=oneshot
ExecStart=/usr/local/hetznerbox_timelapse/main.php snaps:rotate

[Install]
WantedBy=multi-user.target
```

and a timer as `/etc/systemd/system/hetznerboxTimelapse.timer`:

```
# This timer unit is to trig hetznerbox timelapse
# By Aleix Quintana Alsius
# Licensed under GPL V2
#

[Unit]
Description=Rotates the snapshots of hetzner storageboxes
Requires=hetznerboxTimelapse.service

[Timer]
Unit=hetznerboxTimelapse.service
OnCalendar=*-*-* 00:47:00

[Install]
WantedBy=timers.target
```

then reload systemd daemon and start the timer.


### Check command (icinga2)


To be able to run the check and feed icinga it can be like this:

NOTE: If the yml in config dir is used as when is ran in cli directly, storing the password in icinga configuration can be avoid. This example is just to illustrate that it can be ran in different environments.

Command:

```
object CheckCommand "hetzner_snapshots" {
  import "plugin-check-command"
  command =  [ "/usr/local/hetznerbox_timelapse/main.php", "snaps:list", "--check" ]
  vars.retention_daily = ""
  vars.retention_weekly = ""
  vars.retention_monthly = ""
  vars.creds_user = ""
  vars.creds_pw = ""
  vars.creds_box_id = ""
  vars.mountpoint = ""

  env.HB_TIMELAPSE_RETENTION_DAILY = vars.retention_daily
  env.HB_TIMELAPSE_RETENTION_WEEKLY = vars.retention_weekly
  env.HB_TIMELAPSE_RETENTION_MONTHLY = vars.retention_monthly
  env.HB_TIMELAPSE_CREDENTIALS_USER = vars.creds_user
  env.HB_TIMELAPSE_CREDENTIALS_PASSWORD = vars.creds_pw
  env.HB_TIMELAPSE_BOX_ID = vars.creds_box_id
  env.HB_TIMELAPSE_MOUNT_POINT = vars.mountpoint
}

```

host:

```
object Host "HostThatRunsCheck"{
...
  vars.hetzner_snapshots = {
    retention_daily = 2
    retention_weekly = 1
    retention_monthly = 1
    creds_user = "UserfromHetznerSettingsAppsInRobot"
    creds_pw = "blablabla"
    creds_box_id = IDOFBOX
    mountpoint = "USELESSFORTHISCHECK"
  }
...
}
```

Service:

```
apply Service "hetzner box snapshots" {
  import "generic-service"

  check_command = "hetzner_snapshots"
  check_interval = 86400 // 24 hours
  //specify where the check is executed
  command_endpoint = host.vars.client_endpoint // uncomment to ran local instead of master
  
  vars = host.vars.hetzner_snapshots

  assign where host.vars.hetzner_snapshots
}
```


Take care that symfony usually creates the var directory to store cache at root path of the project so maybe a `chown nagios:nagios pathofproject/var` can solve your problems.


## Notes
By now only daily weekly and monthly retentions are possible. 
