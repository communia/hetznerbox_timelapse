#!/usr/bin/env php
<?php
// main.php

require __DIR__.'/vendor/autoload.php';

use App\AppKernel;
use Symfony\Component\Console\Application;
use hetznerboc_timelapse\Command\RotateSnapshotsCommand;

$kernel = new AppKernel('dev', true);
$kernel->boot();
$container = $kernel->getContainer();

$application = $container->get(Application::class);

$application->run();

