<?php
// src/Command/RotateSnapshotsCommand.php
namespace hetznerbox_timelapse\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableCell;
use Symfony\Component\Console\Helper\TableSeparator;

use Hetzner\Robot\Client;

class RotateSnapshotsCommand extends Command
{
  // the name of the command 
  protected static $defaultName = 'snaps:rotate';

  protected $robot;
  protected $sbUser;
  protected $sbPassword;
  protected $boxId;
  protected $mountPoint;
  protected $maxSnaps;

  // the datetime of now
  protected $now;
  // slots to be removed to gain space from untagged snapshot
  protected $snaps_room;
  private $container;
  private $parameters;

  /**
   * @param \Psr\Container\ContainerInterface $container
   *   The container.
   *
   * @throws LogicException When the command name is empty
   */
  public function __construct(\Psr\Container\ContainerInterface $container){
    // injecting additins service as in https://stackoverflow.com/questions/63192252/how-to-get-configuration-parameter-into-a-command
    // Can be done injecting here and adding in services.yml
    $this->container = $container;
    $this->parameters = $container->getParameterBag();
    parent::__construct(self::$defaultName);
  }
  protected function configure(): void
  {

    $this
      ->setDescription('Rotates the snapshots.')
      ->setHelp('This command allows you to rotate the snapshots, as configured in the config.')
      ->addOption('overwrite', null, InputOption::VALUE_NONE, 'If manual snapshots can be overwrite (will exit elsewhere).');
  }

  protected function execute(InputInterface $input, OutputInterface $output): int
  {
    $this->now =  new \DateTime();
    $daily_retentions = $this->parameters->get('hetznerbox_timelapse.retention.daily');
    // same as ^ $this->container->getParameter('hetznerbox_timelapse.retention.daily');
    $weekly_retentions = $this->parameters->get('hetznerbox_timelapse.retention.weekly');
    $monthly_retentions = $this->parameters->get('hetznerbox_timelapse.retention.monthly');
    $this->sbUser = $this->parameters->get('hetznerbox_timelapse.credentials.user');
    $this->sbPassword = $this->parameters->get('hetznerbox_timelapse.credentials.password');
    $this->boxId = $this->parameters->get('hetznerbox_timelapse.box_id');
    $this->mountPoint = $this->parameters->get('hetznerbox_timelapse.mount_point');

    $this->robot =  new Client('https://robot-ws.your-server.de', $this->sbUser, $this->sbPassword);

    // ... put here the code to create the user
    try {
      $storage_box = $this->robot->storageboxGet($this->boxId)->storagebox;
      $sb_plan = substr($storage_box->product, 0, 4);
      $this->maxSnaps = $this->parameters->get('storagebox.' . $sb_plan . '.max_snapshots');
      $snaps = $this->robot->storageboxSnapshotGet($this->boxId);

    }catch (RobotClientException $e)
    {
      $output->writeln('<error>' . $e->getMessage() . '</error>');
      return Command::FAILURE;
    }

    $total_retentions = $daily_retentions + $weekly_retentions + $monthly_retentions;
    if ($total_retentions > $this->maxSnaps) {
      $output->writeln('<error>The desired retention (' . $total_retentions  . ') is greater than the max snapshots allowed in your storage box plan:' . $this->maxSnaps . 
	'. Edit retention configuration to match it.</error>');
      return Command::FAILURE;
    }

    // GET snaps without rotate time comments
    $this->snaps_room = $this->getBiases($snaps, -1);

    if ($total_retentions + count($this->snaps_room) > $this->maxSnaps && !$input->getOption("overwrite")){
      $output->writeln('<error>The desired retention (' . $total_retentions  . ') plus the existing manual snapshot (' . count($this->snaps_room) . 
	') is greater than the maximum snapshots allowed in your storage box plan:' . $this->maxSnaps . 
	'. Edit retention configuration to match it, remove snapshots manually, or add --overwrite flag to ' . 
	' automatically delete any existing manual snapshots as needed to make room.</error>');
      return Command::FAILURE;
    }

    $rotated = false;
    // Check dailies
    $dailies_biases = $this->getBiases($snaps, "ciutage");
    // also as it's the first rotate make room for next snapshots
    $rotated = $rotated or $this->rotateSnaps($dailies_biases, $daily_retentions, "ciutage", $input->getOption("overwrite"));
    // Check weeklies
    $weekly_biases = $this->getBiases($snaps, "ciusemajne");
    $rotated = $rotated or $this->rotateSnaps($weekly_biases, $weekly_retentions, "ciusemajne");
    // Check monthlies
    $monthly_biases = $this->getBiases($snaps, "ciumonate");
    $rotated = $rotated or $this->rotateSnaps($monthly_biases, $monthly_retentions, "ciumonate");

    if ($rotated) {
      $output->writeln('<info>Snapshots rotated!</info>');
      $this->getApplication()->find('snaps:list')->run(new ArrayInput([]), $output);
    } else {
      $output->writeln('<info>Snapshots unchanged</info>');
      $this->getApplication()->find('snaps:list')->run(new ArrayInput([]), $output);
    }

    // this method must return an integer number with the "exit status code"
    return Command::SUCCESS;
  }

  /**
   *  Checks if there are snapshots not done by hetznerbox_timelapse
   *
   *  @var array $snaps
   *    Snapshots.
   */
  protected function existsManualSnapshots($snaps){
    foreach($snaps as $snap){
      if (!in_array( $snap->snapshot->comment, ["ciumonate","ciusemajne","ciutage"])){
	return False;	
      }
    }
    return True;
  }

  /**
   *  Gets the snapshots and the quantity of time for specific retention type.
   *
   *  @var array $snaps
   *    Snapshots.
   *  @var string $retention 
   *    The retention type it could be ciutage, ciusemajne, ciumonate or -1 if wants all others.
   *
   *  @return array
   *    Keyed by name and valued by quantity of retention type (x days or x weeks or x months)
   */
  protected function getBiases($snaps, $retention){
    $biases = [];
    foreach($snaps as $snap){
      if ($snap->snapshot->comment == $retention){
	$snap_time = new \DateTime($snap->snapshot->timestamp);
	$diff =  $this->now->diff($snap_time)->days;
	if ($retention == "ciusemajne") {
	  $diff = intdiv($diff,7);
	}
	if ($retention == "ciumonate") {
	  $diff = intdiv($diff,31);
	}
	$biases[$snap->snapshot->name] = $diff;
      }
      if ($retention === -1  && !in_array( $snap->snapshot->comment, ["ciumonate","ciusemajne","ciutage"])) {
	$biases[] = $snap->snapshot->name;
      }
    }
    return $biases;
  }



  /**
   *  Rotates the snapshots for specific retention type.
   *
   *  @var array $biases
   *    bias array as: ["name" => (integer)quantity of retention units].
   *  @var int retentions
   *    the quantity of $retentions to fill.
   *  @var string $retention
   *    The retention type.
   *  @var bool $overwrite 
   *    If none planned snapshots can be overwrite to make room for snapshots.
   *
   *  @return array
   *    Keyed by name and valued by quantity of retention type (x days or x weeks or x months)
   */
  protected function rotateSnaps($biases, $retentions, $retention_type, $overwrite = false ) {
    $rotated = false;
    $today_retention = false;
    // if there are no previous snapshots and wants $retentions snapshots.
    if (empty($biases) && $retentions > 0 ){
      if ($overwrite && count($this->snaps_room) > 0){
	//make room!
	$this->robot->storageboxSnapshotDelete($this->boxId, array_pop($this->snaps_room));
      }
      // and proceed to create snapshot
      $new_snap = $this->robot->storageboxSnapshotCreate($this->boxId);
      // sleep for 5 seconds if rate limit!!
      sleep(5);
      $this->robot->storageboxSnapshotSetComment($this->boxId, $new_snap->snapshot->name, $retention_type);
      $today_retention = true;
      $rotated = true;
    }
    foreach ($biases as $snapshot_name => $bias){
      if ($bias == 0 ) {
	$today_retention = true;
      }
      if ($bias > $retentions){
	//delete_retention 
	sleep(5);
	$this->robot->storageboxSnapshotDelete($this->boxId, $snapshot_name);
      }
    }

    if (!$today_retention){
      try {
	//create_snapshot and comment with retention_type
	$new_snap = $this->robot->storageboxSnapshotCreate($this->boxId);
	// sleep for 5 seconds if rate limit!!
	sleep(5);
	$this->robot->storageboxSnapshotSetComment($this->boxId, $new_snap->snapshot->name, $retention_type);
      } catch (RobotClientException $e)
      {
	$output->writeln('<error>' . $e->getMessage() . '</error>');
      }
      $rotated = true;
    }
    return $rotated;
  }
}

