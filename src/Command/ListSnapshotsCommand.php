<?php
// src/Command/ListSnapshotsCommand.php
namespace hetznerbox_timelapse\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableCell;
use Symfony\Component\Console\Helper\TableSeparator;

use Hetzner\Robot\Client;

class ListSnapshotsCommand extends Command
{
  // the name of the command 
  protected static $defaultName = 'snaps:list';

  protected $robot;
  protected $sbUser;
  protected $sbPassword;
  protected $boxId;
  protected $mountPoint;
  protected $maxSnaps;

  // the datetime of now
  protected $now;
  // slots to be removed to gain space from untagged snapshot
  protected $snaps_room;
  private $container;
  private $parameters;

  /**
   * @param \Psr\Container\ContainerInterface $container
   *   The container.
   *
   * @throws LogicException When the command name is empty
   */
  public function __construct(\Psr\Container\ContainerInterface $container){
    // injecting additins service as in https://stackoverflow.com/questions/63192252/how-to-get-configuration-parameter-into-a-command
    // Can be done injecting here and adding in services.yml
    $this->container = $container;
    $this->parameters = $container->getParameterBag();
    parent::__construct(self::$defaultName);
  }
  protected function configure(): void
  {

    $this
      ->setDescription('Lists the snapshots.')
      ->setHelp('This command allows you to list the snapshots, as a table, or to feed monitoring software.')
      ->addOption('full', null, InputOption::VALUE_NONE, 'If wants a full properties table.')
      ->addOption('check', null, InputOption::VALUE_NONE, 'A report that will be icinga2 and nagios friendly.');

  }

  protected function execute(InputInterface $input, OutputInterface $output): int
  {
    $this->now =  new \DateTime();
    $daily_retentions = $this->parameters->get('hetznerbox_timelapse.retention.daily.processed');
    // same as ^ $this->container->getParameter('hetznerbox_timelapse.retention.daily.processed');
    $weekly_retentions = $this->parameters->get('hetznerbox_timelapse.retention.weekly.processed');
    $monthly_retentions = $this->parameters->get('hetznerbox_timelapse.retention.monthly.processed');
    $this->sbUser = $this->parameters->get('hetznerbox_timelapse.credentials.user.processed');
    $this->sbPassword = $this->parameters->get('hetznerbox_timelapse.credentials.password.processed');
    $this->boxId = $this->parameters->get('hetznerbox_timelapse.box_id.processed');
    $this->mountPoint = $this->parameters->get('hetznerbox_timelapse.mount_point.processed');

    $this->robot =  new Client('https://robot-ws.your-server.de', $this->sbUser, $this->sbPassword);

    // ... put here the code to create the user
    try {
      $storage_box = $this->robot->storageboxGet($this->boxId)->storagebox;
      $sb_plan = substr($storage_box->product, 0, 4);
      $this->maxSnaps = $this->parameters->get('storagebox.' . $sb_plan . '.max_snapshots');
      $snaps = $this->robot->storageboxSnapshotGet($this->boxId);

    }catch (RobotClientException $e)
    {
      $output->writeln('<error>CRITICAL:' . $e->getMessage() . '</error>');
      return Command::FAILURE;
    }

    $total_retentions = $daily_retentions + $weekly_retentions + $monthly_retentions;
    if ($total_retentions > $this->maxSnaps) {
      $output->writeln('<error>CRITICAL: The desired retention (' . $total_retentions  . ') is greater than the max snapshots allowed in your storage box plan:' . $this->maxSnaps . 
	'. Edit retention configuration to match it.</error>');
      return Command::FAILURE;
    }

    // GET snaps without rotate time comments
    $this->snaps_room = $this->getBiases($snaps, -1);

    if ($total_retentions + count($this->snaps_room) > $this->maxSnaps && !$input->getOption("overwrite")){
      $output->writeln('<error>CRITICAL: The desired retention (' . $total_retentions  . ') plus the existing manual snapshot (' . count($this->snaps_room) . 
	') is greater than the maximum snapshots allowed in your storage box plan:' . $this->maxSnaps . 
	'. Edit retention configuration to match it, remove snapshots manually, or add --overwrite flag to ' . 
	' automatically delete any existing manual snapshots as needed to make room.</error>');
      return Command::FAILURE;
    }

    if ($input->getOption("check")) {
      $biases = [];
      // Check dailies
      $biases["daily"] = $this->getBiases($snaps, "ciutage");
      // Check weeklies
      $biases["weekly"] = $this->getBiases($snaps, "ciusemajne");
      // Check monthlies
      $biases["monthly"] = $this->getBiases($snaps, "ciumonate");
      return $this->checkReport($snaps, $biases, $output);
    } 

    $this->printSnapshots($input, $output);
    // this method must return an integer number with the "exit status code"
    return Command::SUCCESS;
  }

  /**
   *  Checks if there are snapshots not done by hetznerbox_timelapse
   *
   *  @var array $snaps
   *    Snapshots.
   */
  protected function existsManualSnapshots($snaps){
    foreach($snaps as $snap){
      if (!in_array( $snap->snapshot->comment, ["ciumonate","ciusemajne","ciutage"])){
	return False;	
      }
    }
    return True;
  }

  /**
   *  Gets the snapshots and the quantity of time for specific retention type.
   *
   *  @var array $snaps
   *    Snapshots.
   *  @var string $retention 
   *    The retention type it could be ciutage, ciusemajne, ciumonate or -1 if wants all others.
   *
   *  @return array
   *    Keyed by name and valued by quantity of retention type (x days or x weeks or x months)
   */
  protected function getBiases($snaps, $retention){
    $biases = [];
    foreach($snaps as $snap){
      if ($snap->snapshot->comment == $retention){
	$snap_time = new \DateTime($snap->snapshot->timestamp);
	$diff =  $this->now->diff($snap_time)->days;
	if ($retention == "ciusemajne") {
	  $diff = intdiv($diff,7);
	}
	if ($retention == "ciumonate") {
	  $diff = intdiv($diff,31);
	}
	$biases[$snap->snapshot->name] = $diff;
      }
      if ($retention === -1  && !in_array( $snap->snapshot->comment, ["ciumonate","ciusemajne","ciutage"])) {
	$biases[] = $snap->snapshot->name;
      }
    }
    return $biases;
  }

  /**
   * Prints the snapshots table.
   *
   */
  protected function printSnapshots(InputInterface $input, OutputInterface $output){
    $snaps = $this->robot->storageboxSnapshotGet($this->boxId);
    if (count($snaps) < 1){
      $output->writeln('No snapshots there');
      return;
    } 

    $table = new Table($output);
    if ($input->getOption("full")) {
      $full_report = $this->fullReport($snaps, $table);
    }
    else {
      $full_report = $this->shortReport($snaps, $table);
    }
    $table->render();
  }


  /**
   *  Generates a report valid for monitor software as icinga2 or nagios type.
   *
   *  @var array $snaps
   *    Snapshots.
   *  @var array $all_biases
   *    bias array as: [ retention_type => ["name" => (integer)quantity of retention units] ].
   *  @var Symfony\Component\Console\Output\OutputInterface $output
   *    The output interface.
   *
   *  @return int
   *    1 -> Failure , 0 -> Success
   */
  protected function checkReport($snaps,  $all_biases, OutputInterface $output) {
    $general_output = ["code" => 0 , "text" => "" ];
    foreach (["daily", "weekly", "monthly"] as $retention) {
      $bias_check[$retention] = $this->biasCheck($retention, $all_biases[$retention], $output);
      if ($bias_check[$retention]["code"] == 2 && $general_output["code"] == 0 ){
	$general_output["code"] = 2;
      }
      if ($bias_check[$retention]["code"] == 1) {
	$general_output["code"] = 1;
      }
      $general_output["text"] .= $bias_check[$retention]["text"];
    }
    if($general_output["code"] == 1) {
      $output->writeln('<error>CRITICAL:' . $general_output["text"] . '</error>');
      return Command::FAILURE;
    } elseif($general_output["code"] == 2) {
      $output->writeln('<comment>WARNING:' . $general_output["text"] . '</comment>');
      return Command::FAILURE;
    } else {
      $output->writeln('<info>OK:' . $general_output["text"] . '</info>');
      return Command::SUCCESS;
    }
  }    

  /**
   *  Generates the preliminary output of a piece of retention check.
   *
   *  @var string $retention
   *    The retention type.
   *  @var array $biases
   *    bias array as: [ retention_type => ["name" => (integer)quantity of retention units] ].
   *  @var Symfony\Component\Console\Output\OutputInterface $output_int
   *    The output interface.
   *
   *  @return array
   *    The code, and text of the check ["code" => (1 -> Failure , 0 -> Success), 
   *    "text" => "check output" ]
   */
  protected function biasCheck($retention, $biases, OutputInterface $output_int){
    $output = [ "code" => 0, "text" => "No " . $retention . " snapshots defined." ]; 
    if ($this->parameters->get('hetznerbox_timelapse.retention.' . $retention ) > 0) {
      $output = [ 
	"code" => 2, 
	"text" => $retention . " retentions defined but not yet done. "
      ];
      $partial_message = '<comment>[WARNING]' . $output['text'] . '</comment>';
      foreach ($biases as $name => $bias){
	if ($bias > 1) {
  	  $output = [ 
	    "code" =>  1 , 
	    "text" => "All $retention snapshots are older than configured. "  
	  ];
         $partial_message = '<error>[CRITICAL]' . $output['text'] . '</error>'; 
	}
	if ($bias == 1 or $bias == 0 ) {
	  $output =  [
	    "code" => 0,
	    "text" => "Last $retention snapshot has been processed. "
	  ];
          $partial_message = '<info>[OK]' . $output['text'] . '</info>'; 
	}
      }
      $output_int->writeln($partial_message);
    }
    return $output;
  }

  /**
   *  Generates a short table report.
   *
   *  @var array $snaps
   *    Snapshots.
   *  @var Symfony\Component\Console\Helper\Table $table
   *    The table to set rows.
   */
  protected function shortReport($snaps, $table) {
    $headers = ["name", "size", "comment", "mount"];
    $rows = [];
    foreach ($snaps as $snap){
      $rows[] = [
	$snap->snapshot->name,
	$snap->snapshot->size,
	$snap->snapshot->comment,
	$this->mountPoint . "/.zfs/snapshot/" . $snap->snapshot->name
      ];
    }
    $table
      ->setHeaders($headers)
      ->setRows($rows);

  }

  /**
   *  Generates a full table report.
   *
   *  @var array $snaps
   *    Snapshots.
   *  @var Symfony\Component\Console\Helper\Table $table
   *    The table to set rows.
   */
  protected function fullReport($snaps, $table) {
    $headers = array_keys(get_object_vars($snaps[0]->snapshot));
    $rows = [];
    foreach ($snaps as $snap){
      $rows[] = new TableSeparator();
      $rows[]= [new TableCell( "Mounted at: " .
	$this->mountPoint .
	"/.zfs/snapshot/" .
	$snap->snapshot->name, ['colspan' => count((array)$snap->snapshot) ])];
      $rows[] = new TableSeparator();
      $rows[]= (array)$snap->snapshot;
    }
    $table
      ->setHeaders($headers)
      ->setRows($rows);

  }

}


